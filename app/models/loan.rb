class Loan < ApplicationRecord
	belongs_to :user
	belongs_to :book

	before_validation :set_state, on: :create

##### BUSCADOR
	def self.search(search)
  		#where("user_id LIKE ?", "%#{search}%")
  		estados = where("state LIKE ?", "%#{search}%")
  		ids = where("id LIKE ?", "%#{search}%")
  		#names = Loan.includes(:user).where("user.last_name LIKE ?", "%#{search}%")
		Loan.from("(#{estados.to_sql} UNION #{ids.to_sql}) AS loans")

  	end
##### ESTADO POR DEFECTO
	def set_state
	    self.state = "actived"
	end
##### SEGÚN EL VALOR QUE SE INGRESA ES LO QUE SE VE
	def estado
		estados = {"actived" => "Activo", "expired" => "Expirado", "finished" => "Finalizado"}
		estados[self.state]
	end

##### CAMBIA LOS ESTADOS VENCIDOS AUTOMATICAMENTE
	def is_expired?
		if self.final_date < DateTime.now && self.state == "actived"
			self.state = "expired"
			self.save
		end
		self.state == "expired"
	end

end
