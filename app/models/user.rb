class User < ApplicationRecord
  
	has_secure_password
	has_many :loans
	has_many :books, through: :loans

	

	def self.search(search)
  		#where("last_name LIKE ?", "%#{search}%")
  		apellidos = where("last_name LIKE ?", "%#{search}%")
  		dnis = where("dni LIKE ?", "%#{search}%")
  		tipos = where("role LIKE ?", "%#{search}%")
		User.from("(#{apellidos.to_sql} UNION #{dnis.to_sql} UNION #{tipos.to_sql}) AS users")
	end	

	def student?
    	self.role == 'Alumno'
  	end

  	def librarian?
   		self.role == 'Bibliotecario'
 	end

 	def preceptor?
   		self.role == 'Preceptor'
 	end

 	def professor?
   		self.role == 'Docente'
 	end

##### SEGÚN EL VALOR QUE SE INGRESA ES LO QUE SE VE
	#def rol
	#	roles = {"librarian" => "Bibliotecario", "preceptor" => "Preceptor", "student" => "Alumno", "professor" => "Docente"}
	#	roles[self.role]
	#end

 	
	
end
