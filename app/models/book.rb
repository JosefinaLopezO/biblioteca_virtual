class Book < ApplicationRecord
	has_many :loans
	has_many :users, through: :loans

	before_validation :set_state, on: :create

##### DISPONIBILIDAD DEL LIBRO SEGÚN PRÉSTAMO ACTIVO
	def available?
		(loans.where(state: "actived").or(loans.where(state: "expired"))).count == 0
	end

##### DISPONIBILIDAD DEL LIBRO
	def available
		if self.available?
			self.state = "available"
			dispo = {"available" => "Disponible"}
			dispo[self.state]
		else
			self.state = "not available"
			no_dispo = {"not available" => "No Disponible"}
			no_dispo[self.state]
		end
	end

##### ESTADO POR DEFECTO
	def set_state
	    self.state = "available"
	    dispo = {"available" => "Disponible"}
		dispo[self.state]
	end

##### SEGÚN EL VALOR QUE SE INGRESA ES LO QUE SE VE
	#def estado
	#	estados = {"available" => "Disponible", "not available" => "No Disponible"}
	#	estados[self.state]
	#end


	def self.search(search)
  		authors = where("author LIKE ?", "%#{search}%")
  		names = where("name LIKE ?", "%#{search}%")
  		isbns = where("isbn LIKE ?", "%#{search}%")
		Book.from("(#{authors.to_sql} UNION #{names.to_sql} UNION #{isbns.to_sql}) AS books")

	end	

end


