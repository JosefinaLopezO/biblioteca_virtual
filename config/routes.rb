Rails.application.routes.draw do
  get 'welcome/index'
  root 'welcome#index'
  get 'loans/new'
  resources :books
  resources :users
  resources :loans



  # Session routes
	get 'login' => 'session#new'
	post 'login' => 'session#create'
	delete 'logout' => 'session#destroy'
	get 'logout' => 'session#destroy'

end
