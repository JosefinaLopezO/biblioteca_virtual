class CreateLoans < ActiveRecord::Migration[6.0]
  def change
    create_table :loans do |t|
      t.datetime :initiation_date
      t.datetime :final_date
      t.string :state

      t.timestamps
    end
  end
end
